# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="jmartindf"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(gitfast brew bundler cap github osx screen terminalapp)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=10000
export PATH=$HOME/local/bin:/opt/local/bin:/opt/local/sbin:/usr/local/bin:/usr/local/sbin:$HOME/scripts:/usr/local/mysql/bin:$PATH
export PATH=$PATH:$JRUBY_HOME/bin
export EDITOR=vim
export VISUAL=vim
export JAVA_HOME='/System/Library/Frameworks/JavaVM.framework/Home'
export ANT_HOME='/Developer/Java/Ant'
export LC_CTYPE=en_US.UTF-8
# PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

setopt histignoredups
setopt HIST_IGNORE_SPACE
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_NO_FUNCTIONS
setopt HIST_SAVE_NO_DUPS

alias home='ssh -t home "tmux attach -t def || tmux new -s def"'
alias tmwork='tmux attach -t work || tmux new -s work'
alias copytv='ssh -t chome /Users/cmartin/scripts/copytv.sh'
alias litho='ssh -t litho'
alias mark='ssh jmartin@mark.desertflood.com'
alias log="~/scripts/logtodayone.rb"

autoload -U zmv
alias mmv='noglob zmv -W'

function sc {
	ruby -e 'puts ARGV[0].capitalize' "$1"
}
function mobi {
	input=$1;
	shift;
	base=`basename "${input}" .prc`;
	mobi2mobi "${input}" --outfile "${base}.mobi" "$@";
}

if [ -f $HOME/.bash_local ];
then
	source $HOME/.bash_local
fi


# Evaluate system PATH
if [ -x /usr/libexec/path_helper ]; then
    eval `/usr/libexec/path_helper -s`
fi
