
map <leader>sp :e ~\Desktop\Work\ Space\Scratch\ Pad.txt<cr>
map <leader>ol :e ~\Dropbox\Joe\dailylog.txt<cr>

if has("win32")
	set directory=$HOME/vimfiles/tmp//  "Swap files for Windows
	set backupdir=$HOME/vimfiles/tmp//
	source $VIMRUNTIME/mswin.vim
	behave mswin
endif

