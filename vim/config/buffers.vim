
" Buffer / File shortcuts
map <F2> :NERDTreeToggle<CR>
map <F5> :buffers<CR>:buffer<Space>
set wildchar=<Tab> wildmenu wildmode=full
set wildcharm=<C-Z>
nnoremap <F10> :b <C-Z>

