
" UNCOMMENT TO USE
set tabstop=2                    " Global tab width.
set shiftwidth=2                 " And again, related.
set softtabstop=2                " And again, related
set expandtab                    " Use spaces instead of tabs

" Text settings
function! SetTextSettings(md)
  setlocal shiftwidth=2
  setlocal tabstop=2
  setlocal wrap
  setlocal linebreak
  setlocal encoding=utf-8
  if 1==a:md
    set columns=90
    set filetype=mkd
  endif
endfunction
nmap <leader>ts :call SetTextSettings(0)<cr>
nmap <leader>ms :call SetTextSettings(1)<cr>

" Apply text settings to text buffers
autocmd BufRead,BufNewFile   *.txt setfiletype text
autocmd BufRead,BufNewFile   *.txt,*.markdown,*.mdown,*.mmd,*.mkd call SetTextSettings(0)
autocmd BufEnter   *.txt,*.markdown,*.mdown,*.mmd,*.mkd call UniCycleOn()
autocmd BufLeave * call UniCycleOn()
autocmd BufLeave * call UniCycleOff()

" Wrap mode friendly mappings
nmap j gj
nmap k gk

" Convert a line to Title Case
nmap <leader>tc :s/\<\(\w\)\(\w*\)\>/\u\1\L\2/g<cr>:let @/ = ""<cr>

" Open in Marked command
nnoremap <leader>m :silent !open -a Marked.app '%:p'<cr>

" Add a line to a Day One log buffer
if has("win32")
  nmap <leader>log o====<cr>[<C-R>=strftime("%Y-%m-%d %#I:%M%p")<CR>] 
else
  nmap <leader>log o====<cr>[<C-R>=strftime("%Y-%m-%d %-H:%-M%p")<CR>] 
endif

