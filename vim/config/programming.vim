
" Automatic fold settings for specific files. Uncomment to use.
" autocmd FileType ruby setlocal foldmethod=syntax
" autocmd FileType css  setlocal foldmethod=indent shiftwidth=2 tabstop=2

" Programming settings
autocmd BufRead,BufNewFile   *.js,*.rb setlocal shiftwidth=2
autocmd BufRead,BufNewFile   *.js,*.rb setlocal tabstop=2
autocmd BufRead,BufNewFile   *.js,*.rb setlocal softtabstop=2

