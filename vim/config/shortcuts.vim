
" Select the entire line (minus the EOL character)
map <leader>vls ^v$

" Easily clear search highlights
nnoremap <s-h> :let @/ = ""<cr>

