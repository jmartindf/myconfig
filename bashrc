export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=10000
export PS1='\[\033[G\]\[\033[01;32m\]\h:\[\033[01;34m\]\w\$ \[\033[00m\]'
export PATH=$HOME/local/bin:/opt/local/bin:/opt/local/sbin:/usr/local/bin:/usr/local/sbin:$HOME/scripts:/usr/local/mysql/bin:$PATH
export PATH=$PATH:$JRUBY_HOME/bin
export EDITOR=vim
export VISUAL=vim
export JAVA_HOME='/System/Library/Frameworks/JavaVM.framework/Home'
export ANT_HOME='/Developer/Java/Ant'
export JRUBY_HOME='/opt/jruby'
export MANPATH=${MANPATH}:/opt/local/man
export LC_CTYPE=en_US.UTF-8
shopt -s histappend

#if [ -f /opt/local/etc/bash_completion ]; then
#    . /opt/local/etc/bash_completion
#fi

umask 002

alias home='ssh -t home screen -Rd'
alias copytv='ssh -t chome /Users/cmartin/scripts/copytv.sh'
alias litho='ssh -t litho'
alias mark='ssh jmartin@mark.desertflood.com'
alias ls='ls -G'
alias riht='ruby -e"%x{ri -f html #{ARGV[0]}>/tmp/ri.html; open -a Safari /tmp/ri.html}"'
alias syncarchives='rsync -rltvz --exclude '.DS_Store' /Users/jmartin/Archives wismartins@wismartins.strongspace.com:/home/wismartins'
alias archive='/usr/bin/ruby /Users/jmartin/development/del2filer.rb'
alias log="~/scripts/logtodayone.rb"

function joinchapter {
	mpgjoin $1*/*.mp3 -o ChapterFiles/$1.mp3
}

function budgetss {
	/Users/jmartin/scripts/budget -f $1 | awk -F\t '{print $2;}'
}
function dbudget {
        /Users/jmartin/scripts/budget -d -f $1
}
function sumbudget {
        /Users/jmartin/scripts/budget -f $1
}

function sc {
	ruby -e 'puts ARGV[0].capitalize' "$1"
}
function mobi {
	input=$1;
	shift;
	base=`basename "${input}" .prc`;
	mobi2mobi "${input}" --outfile "${base}.mobi" "$@";
}

function passgen25 {
	var=`echo $1 | md5`
	eval echo "\${var%${var#????????}}"
}

if [ -f $HOME/.bash_local ];
then
	source $HOME/.bash_local
fi

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
