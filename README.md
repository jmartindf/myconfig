This is a place to store my Bash, VIM, etc config files across machines.

Use `setup.sh` to make links to the following files

~/.vim   
~/.vimrc   
~/.gvimrc   
~/.zshrc   
~/.oh-my-zsh   
~/.zprofile   
~/.tmux.conf   

