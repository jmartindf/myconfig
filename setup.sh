#!/bin/sh
# Setup symlinks
# Remove the existing config, assume it's already a symlink
# ####DANGEROUS!!!!####

rm ~/.vim
rm ~/.vimrc
rm ~/.gvimrc
rm ~/.zshrc
rm ~/.oh-my-zsh
rm ~/.zprofile
rm ~/.tmux.conf
rm ~/Library/"Application Support/Sublime Text 3"/Packages/User
ln -s "${1}/vim" ~/.vim
ln -s "${1}/vim/vimrc" ~/.vimrc
ln -s "${1}/vim/gvimrc" ~/.gvimrc
ln -s "${1}/zshrc" ~/.zshrc
ln -s "${1}/zprofile" ~/.zprofile
ln -s "${1}/oh-my-zsh" ~/.oh-my-zsh
ln -s "${1}/tmux.conf" ~/.tmux.conf
ln -s "${1}/SublimeText3" ~/Library/"Application Support/Sublime Text 3"/Packages/User

